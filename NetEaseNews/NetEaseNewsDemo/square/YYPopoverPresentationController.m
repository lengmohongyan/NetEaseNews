//
//  YYPopoverPresentationController.m
//  NetEaseNewsDemo
//
//  Created by mac on 15/6/2.
//  Copyright (c) 2015年 Lining. All rights reserved.
//

#import "YYPopoverPresentationController.h"
@interface YYPopoverPresentationController()
///遮罩视图
@property (nonatomic,strong) UIView * dummyView;
@end

@implementation YYPopoverPresentationController
/// 转场控制器的构造方法
-(instancetype)initWithPresentedViewController:(UIViewController *)presentedViewController presentingViewController:(UIViewController *)presentingViewController
{
    self = [super initWithPresentedViewController:presentedViewController presentingViewController:presentingViewController];
    return self;
}
///布局容器视图
-(void)containerViewWillLayoutSubviews
{
    [super containerViewWillLayoutSubviews];
    self.dummyView.frame = self.containerView.bounds;
    [self.containerView insertSubview:_dummyView atIndex:0];
    CGRect rect = [UIScreen mainScreen].bounds;
    float  width = rect.size.width;
    float  height = rect.size.height;
    self.presentedView.frame =CGRectMake(0, 64, width, height-64);
}
///遮罩视图
-(UIView *)dummyView
{
  if (_dummyView == nil)
  {
       self.dummyView = [[UIView alloc] init];
       UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dummyViewClick)];
      [self.dummyView addGestureRecognizer:tap];
       self.dummyView.backgroundColor = [UIColor clearColor];
  }
    return _dummyView;
    
}
///点击返回首页
-(void)dummyViewClick
{
    
    [self.presentedViewController dismissViewControllerAnimated:NO completion:nil];
}


@end
