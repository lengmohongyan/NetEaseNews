//
//  YYSettingViewController.m
//  YY
//
//  Created by mac on 15/5/30.
//  Copyright (c) 2015年 mac. All rights reserved.
//

#import "YYSettingViewController.h"

@interface YYSettingViewController ()

@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *functionView;
///第一个 搜索
@property (weak, nonatomic) IBOutlet UIButton *searchButton;
///第二个 上头条
@property (weak, nonatomic) IBOutlet UIButton *gotoHeadrButton;
///第三个 离线
@property (weak, nonatomic) IBOutlet UIButton *downloadButton;
/// 第四个 夜间
@property (weak, nonatomic) IBOutlet UIButton *nightButton;
/// 第五个
@property (weak, nonatomic) IBOutlet UIButton *QRcodeButton;
/// 第六个 邀请好友
@property (weak, nonatomic) IBOutlet UIButton *friendButton;


@end

@implementation YYSettingViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

}

///播放六个按钮的动画
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self animating];
    
}

///六个按钮的动画
-(void)animating
{
    [UIView animateWithDuration:0.35 animations:^{
        self.searchButton.transform = CGAffineTransformMakeScale(0.6, 0.6);
        self.downloadButton.transform = CGAffineTransformMakeScale(0.6, 0.6);
        self.gotoHeadrButton.transform = CGAffineTransformMakeScale(0.6, 0.6);
        self.nightButton.transform = CGAffineTransformMakeScale(0.6, 0.6);
        self.QRcodeButton.transform = CGAffineTransformMakeScale(0.6, 0.6);
        self.friendButton.transform = CGAffineTransformMakeScale(0.6, 0.6);
        [self.functionView enumerateObjectsUsingBlock:^(UIView* view, NSUInteger idx, BOOL *stop) {
            view.transform = CGAffineTransformMakeScale(1.1, 1.1);
        }];
        
    } completion:^(BOOL finished) {
        
        [UIView animateWithDuration:0.35 animations:^{
        
        self.searchButton.transform = CGAffineTransformMakeScale(1.0, 1.0);
        self.downloadButton.transform = CGAffineTransformMakeScale(1.0, 1.0);
        self.gotoHeadrButton.transform = CGAffineTransformMakeScale(1.0, 1.0);
        self.nightButton.transform = CGAffineTransformMakeScale(1.0, 1.0);
        self.QRcodeButton.transform = CGAffineTransformMakeScale(1.0, 1.0);
        self.friendButton.transform = CGAffineTransformMakeScale(1.0, 1.0);

        [self.functionView enumerateObjectsUsingBlock:^(UIView * view, NSUInteger idx, BOOL *stop) {
            view.transform = CGAffineTransformMakeScale(1.0, 1.0);
        }];
        }];
    }];
    
}

@end
