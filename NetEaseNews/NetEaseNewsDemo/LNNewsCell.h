//
//  LNNewsCell.h
//  NetEaseNewsDemo
//
//  Created by Lining on 15/5/11.
//  Copyright (c) 2015年 Lining. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LNNews;
@interface LNNewsCell : UITableViewCell
/// 模型
@property (nonatomic, strong) LNNews *news;

/**
 *  根据news模型设置cell的可重用标示符
 */
+ (NSString *)cellReuseIdWithNews:(LNNews *)news;

/**
 *  根据news模型设置行高
 */
+ (CGFloat)rowHeightWithNews:(LNNews *)news;
@end
