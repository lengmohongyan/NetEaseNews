//
//  ZJRefreshView.h
//  NetEaseNewsDemo
//
//  Created by Xwoder on 15/6/2.
//  Copyright (c) 2015年 Lining. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZJRefreshView : UIView

- (void)start;
- (void)stop;
@end
