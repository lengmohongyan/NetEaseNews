//
//  LNMainTabBarController.m
//  NetEaseNewsDemo
//
//  Created by Lining on 15/5/11.
//  Copyright (c) 2015年 Lining. All rights reserved.
//

#import "LNMainTabBarController.h"

@interface LNMainTabBarController ()

@end

@implementation LNMainTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addChildViewControllers];
}

#pragma mark - 添加子视图控制器
/**
 *  添加子视图控制器
 */
- (void)addChildViewControllers {
    
    self.tabBar.tintColor = [UIColor redColor];
    [self addChildViewControllerWithSBName:@"News" andImgName:@"tabbar_icon_news" andTitle:@"新闻"];
    [self addChildViewControllerWithSBName:@"Reading" andImgName:@"tabbar_icon_reader" andTitle:@"阅读"];
    [self addChildViewControllerWithSBName:@"VideoAndListenFather" andImgName:@"tabbar_icon_media" andTitle:@"视听"];
    [self addChildViewControllerWithSBName:@"Found" andImgName:@"tabbar_icon_found" andTitle:@"发现"];
    [self addChildViewControllerWithSBName:@"Me" andImgName:@"tabbar_icon_me" andTitle:@"我"];
}

- (void)addChildViewControllerWithSBName:(NSString *)sbName andImgName:(NSString *)imgName andTitle:(NSString *)title {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:sbName bundle:nil];
    UINavigationController *nav = sb.instantiateInitialViewController;
    NSString *norImgName = [NSString stringWithFormat:@"%@_normal", imgName];
    NSString *selectImgName = [NSString stringWithFormat:@"%@_highlight", imgName];
    nav.tabBarItem.image = [UIImage imageNamed:norImgName];
    nav.tabBarItem.selectedImage = [UIImage imageNamed:selectImgName];
    nav.title = title;
    nav.topViewController.title = title;
    [self addChildViewController:nav];
}


@end
