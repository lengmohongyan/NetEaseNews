//
//  LNNewsTableViewController.h
//  NetEaseNewsDemo
//
//  Created by Lining on 15/5/11.
//  Copyright (c) 2015年 Lining. All rights reserved.
//

#import "LNBaseTableViewController.h"

@interface LNNewsTableViewController : LNBaseTableViewController
/// 频道
@property (nonatomic, copy) NSString *urlStr;
@end
