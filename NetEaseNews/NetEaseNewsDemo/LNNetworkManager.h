//
//  LNNetworkManager.h
//  NetEaseNewsDemo
//
//  Created by Lining on 15/5/11.
//  Copyright (c) 2015年 Lining. All rights reserved.
//

#import "AFHTTPSessionManager.h"

@interface LNNetworkManager : AFHTTPSessionManager

/** 定义一个网络管理工具单例方法 */
+ (instancetype)sharedManager;

@end
