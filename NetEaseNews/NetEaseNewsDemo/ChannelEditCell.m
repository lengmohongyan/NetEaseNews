//
//  ChannelEditCell.m
//  ChannelEdit
//
//  Created by Lining on 15/6/2.
//  Copyright (c) 2015年 lengmolehongyan. All rights reserved.

//  cell 的大小70 x 30
//

#import "ChannelEditCell.h"
#import "LNChannel.h"

@interface ChannelEditCell ()

/**
 *  频道编辑按钮
 */
@property (weak, nonatomic) IBOutlet UIButton *channelEditButton;

/**
 *  点击删除按钮
 */
- (IBAction)deleteButtonClick:(UIButton *)sender;

@end

@implementation ChannelEditCell

- (void)setChannel:(LNChannel *)channel {
    _channel = channel;
    
    [_channelEditButton setTitle:channel.tname forState:UIControlStateNormal];
}


- (IBAction)deleteButtonClick:(UIButton *)sender {
    
    if ([self.delegate respondsToSelector:@selector(channelEditCell:didClickDeleteButton:)]) {
        [self.delegate channelEditCell:self didClickDeleteButton:sender];
    }
}
@end
