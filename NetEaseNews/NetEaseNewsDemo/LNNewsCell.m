//
//  LNNewsCell.m
//  NetEaseNewsDemo
//
//  Created by Lining on 15/5/11.
//  Copyright (c) 2015年 Lining. All rights reserved.
//

#import "LNNewsCell.h"
#import "LNNews.h"
#import <UIImageView+AFNetworking.h>
#define kImgExtraRH 118
#define kImgBigH    182
#define kImgNorH    82
#define kImgExtraCellID @"NewsCellExtra"
#define kImgBigCellID   @"NewsCellImgType"
#define kImgNorCellID   @"NewsCell"

@interface LNNewsCell ()
/// 小图 正常图片
@property (weak, nonatomic) IBOutlet UIImageView *iconView;
/// 标题
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
/// 摘要
@property (weak, nonatomic) IBOutlet UILabel *digestLabel;
/// 回帖数
@property (weak, nonatomic) IBOutlet UIButton *replyCountLabel;
/// 多图数组
@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *imgExtra;
@end

@implementation LNNewsCell

- (void)awakeFromNib {
    // 设置摘要的最大宽度
//    self.digestLabel.preferredMaxLayoutWidth = [UIScreen mainScreen].bounds.size.width - self.digestLabel.frame.origin.x - 8;
}

+ (CGFloat)rowHeightWithNews:(LNNews *)news {
    if (news.imgextra.count == 2) {
        return kImgExtraRH;
    } else if (news.imgType) {
        return kImgBigH;
    }
    return kImgNorH;
}

+ (NSString *)cellReuseIdWithNews:(LNNews *)news {
    if (news.imgextra.count == 2) {
        return kImgExtraCellID;
    } else if (news.imgType) {
        return kImgBigCellID;
    }
    return kImgNorCellID;
}

- (void)setNews:(LNNews *)news {
    _news = news;
    self.titleLabel.text = news.title;
    self.replyCountLabel.hidden = news.replyCount == 0;
    self.digestLabel.text = news.digest;
    [self.replyCountLabel setTitle:[NSString stringWithFormat:@"%zd跟帖", news.replyCount] forState:UIControlStateNormal];
    // 异步加载图像之前，清空图像
    self.iconView.image = nil;
    if (news.imgextra.count == 2) {
        __block NSInteger index = 0;
        [self.imgExtra enumerateObjectsUsingBlock:^(UIImageView *imgView, NSUInteger idx, BOOL *stop) {
            imgView.image = nil;
            [imgView setImageWithURL:[NSURL URLWithString:news.imgextra[index][@"imgsrc"]]];
            index++;
        }];
    }
    [self.iconView setImageWithURL:[NSURL URLWithString:news.imgsrc]];
}

@end
