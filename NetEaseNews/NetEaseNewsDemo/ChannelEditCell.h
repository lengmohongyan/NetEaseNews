//
//  ChannelEditCell.h
//  ChannelEdit
//
//  Created by Lining on 15/6/2.
//  Copyright (c) 2015年 lengmolehongyan. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LNChannel, ChannelEditCell;

@protocol ChannelEditCellDelegate <NSObject>

@optional
/**
 *  频道编辑cell移动代理方法
 */
- (void)channelEditCell:(ChannelEditCell *)cell didClickDeleteButton:(UIButton *)deleteButton;

@end

@interface ChannelEditCell : UICollectionViewCell

/**
 *  删除按钮
 */
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;

/**
 *  频道模型
 */
@property (nonatomic, strong) LNChannel *channel;

/**
 *  代理属性
 */
@property (nonatomic, weak) id<ChannelEditCellDelegate> delegate;

@end
