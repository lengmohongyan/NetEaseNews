//
//  ZJSubscribeList.h
//  NetEaseNewsDemo
//
//  Created by Xwoder on 15/6/4.
//  Copyright (c) 2015年 Lining. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZJSubscribeList : NSObject

// 名称
@property (copy, nonatomic) NSString *tname;
// 订阅数
@property (copy, nonatomic) NSString *subnum;
// 图标名称
@property (copy, nonatomic) NSString *img;

- (instancetype)initWithDictionary:(NSDictionary *)dict;
+ (instancetype)listWithDictionary:(NSDictionary *)dict;

+ (NSArray *)listWithDictArray:(NSArray *)dictArray;

@end
