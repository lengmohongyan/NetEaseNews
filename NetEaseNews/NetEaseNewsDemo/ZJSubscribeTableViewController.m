//
//  ZJSubscribeTableViewController.m
//  NetEaseNewsDemo
//
//  Created by Xwoder on 15/6/4.
//  Copyright (c) 2015年 Lining. All rights reserved.
//

#import "ZJSubscribeTableViewController.h"
#import "ZJSubscribe.h"
#import "ZJSubscribeList.h"


#define SUBSCRIBE_READ_ALL @"http://c.m.163.com/nc/topicset/ios/v4/subscribe/read/all.html"

@interface ZJSubscribeTableViewController () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *leftTableView;
@property (weak, nonatomic) IBOutlet UITableView *rightTableView;


/// 数据源 - 全体数据
@property (strong, nonatomic) NSArray *subscribes;

@property (strong, nonatomic) NSArray *rightSubscribe;

@end

@implementation ZJSubscribeTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.title = @"添加订阅";

    [ZJSubscribe loadSubscribesWithURLStr:SUBSCRIBE_READ_ALL finished:^(NSArray *subscribes) {
        self.subscribes = subscribes;
    }];
}

#pragma mark - Table View Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.leftTableView) {
        return self.subscribes.count;
    } else if (tableView == self.rightTableView) {
        NSIndexPath *selectedIndexPath = [self.leftTableView indexPathForSelectedRow];
        NSUInteger selectedRow = selectedIndexPath.row;
        ZJSubscribe *subscribe = self.subscribes[selectedRow];
        NSArray *array = subscribe.tList;
        return array.count;
    } else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *leftTableViewReuseIdentifier = @"leftCell";
    static NSString *rightTableViewReuseIdentifier = @"rightCell";
    
    // 左边tableView的cell
    if (tableView == self.leftTableView) {
        // 取得数据源
        ZJSubscribe *subscribe = self.subscribes[indexPath.row];
        UITableViewCell *leftCell = [tableView dequeueReusableCellWithIdentifier:leftTableViewReuseIdentifier];
        leftCell.textLabel.text = subscribe.cName;
        leftCell.textLabel.textAlignment = NSTextAlignmentCenter;
        return leftCell;
    } else if (tableView == self.rightTableView) {
        UITableViewCell *rightCell = [tableView dequeueReusableCellWithIdentifier:rightTableViewReuseIdentifier];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeContactAdd];
        button.tintColor = [UIColor redColor];
        rightCell.accessoryView = button;

        NSIndexPath *selectedIndexPath = [self.leftTableView indexPathForSelectedRow];
        NSUInteger selectedRow = selectedIndexPath.row;
        ZJSubscribe *subscribe = self.subscribes[selectedRow];
        NSArray *array = subscribe.tList;
        ZJSubscribeList *list = array[indexPath.row];
        rightCell.textLabel.text = list.tname;
        rightCell.detailTextLabel.text = list.subnum;
        return rightCell;
    } else {
        return nil;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.leftTableView) {
        
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        cell.textLabel.textColor = [UIColor redColor];
        cell.textLabel.font = [UIFont systemFontOfSize:18];
        // 左侧tableView
        [self.rightTableView reloadData];
    } else if (tableView == self.rightTableView) {
        // 右侧

    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.leftTableView) {
        
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        cell.textLabel.textColor = [UIColor blackColor];
        cell.textLabel.font = [UIFont systemFontOfSize:16];
        // 左侧tableView
        [self.rightTableView reloadData];
    } else if (tableView == self.rightTableView) {
        // 右侧
        
    }
}

#pragma mark - 懒加载
- (void)setSubscribes:(NSArray *)subscribes {
    _subscribes = subscribes;
    
    NSLog(@"%@", subscribes);
    
    [self.leftTableView reloadData];
    [self.rightTableView reloadData];
    
    [self.leftTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:NO scrollPosition:UITableViewScrollPositionTop];
    [self tableView:self.leftTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
}




@end
