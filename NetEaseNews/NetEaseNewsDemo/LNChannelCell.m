//
//  LNChannelCell.m
//  NetEaseNewsDemo
//
//  Created by Lining on 15/5/11.
//  Copyright (c) 2015年 Lining. All rights reserved.
//

#import "LNChannelCell.h"
#import "LNNewsTableViewController.h"

@interface LNChannelCell ()
@end

@implementation LNChannelCell
/**
 *  添加表视图控制器的根视图
 */
- (void)awakeFromNib {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"News" bundle:nil];
    self.newsVC = [sb instantiateViewControllerWithIdentifier:@"NewsVC"];
    [self addSubview:self.newsVC.view];
}

/**
 *  布局子控件
 */
- (void)layoutSubviews {
    [super layoutSubviews];
    self.newsVC.view.frame = self.bounds;
}

- (void)setUrlStr:(NSString *)urlStr {
    _urlStr = urlStr.copy;
    self.newsVC.urlStr = urlStr;
}
@end
