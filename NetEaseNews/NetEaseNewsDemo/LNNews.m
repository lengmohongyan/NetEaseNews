//
//  LNNews.m
//  NetEaseNewsDemo
//
//  Created by Lining on 15/5/11.
//  Copyright (c) 2015年 Lining. All rights reserved.
//

#import "LNNews.h"
#import "LNNetworkManager.h"
#import "NSObject+LNExtension.h"

@implementation LNNews

+ (void)loadNewsListWithURLStr:(NSString *)urlStr finished:(void (^)(NSArray *))finished {
    
    // 断言提示必须传入回调参数
    NSAssert(finished, @"必须传入完成回调");
    [[LNNetworkManager sharedManager] GET:urlStr parameters:nil success:^(NSURLSessionDataTask *task, NSDictionary *responseObject) {
        
        NSArray *newsArray = responseObject[responseObject.keyEnumerator.nextObject];
        NSMutableArray *newsList = [NSMutableArray arrayWithCapacity:newsArray.count];
        [newsArray enumerateObjectsUsingBlock:^(NSDictionary *dict, NSUInteger idx, BOOL *stop) {
           
            [newsList addObject:[self objectWithDict:dict]];
        }];
        // 完成回调
        finished(newsList.copy);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@", error);
    }];
}

@end
