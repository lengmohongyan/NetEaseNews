//
//  ZJRefreshView.m
//  NetEaseNewsDemo
//
//  Created by Xwoder on 15/6/2.
//  Copyright (c) 2015年 Lining. All rights reserved.
//

#import "ZJRefreshView.h"

@interface ZJRefreshView ()

@property (weak, nonatomic) IBOutlet UIImageView *circleView;

/** 是否正在播放 */
@property (nonatomic, assign) BOOL isplay;
@property (nonatomic, strong) NSTimer *timer;
@end

@implementation ZJRefreshView

-(void)start {
    _timer = [[NSTimer alloc] initWithFireDate:nil interval:1.2 target:self selector:@selector(starting) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
}


- (void)starting {

    /// 圆圈 顶 -- 下
    CABasicAnimation * animate = [CABasicAnimation animationWithKeyPath:@"transform.translation"];
    animate.fromValue = [NSValue valueWithCGPoint:CGPointMake(0, -12)];
    animate.toValue = [NSValue valueWithCGPoint:CGPointMake(0, 12)];
    
    // 圆圈小到大
    CABasicAnimation *animate2 = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    animate2.fromValue = @(0.25);
    animate2.toValue = @(1);
    animate2.duration = 0.6;
    // 圆圈大到小
    CABasicAnimation *animate3 = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    animate3.fromValue = @(1);
    animate3.toValue = @(0.25);
    animate3.duration = 0.6;
    /// 圆圈上到中 小变大
    CAAnimationGroup *group = [CAAnimationGroup animation];
    group.animations = @[animate,animate2];
    group.duration = 1.2;
    
    [_circleView.layer addAnimation:group forKey:nil];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        /// 圆圈中到下 大变小
        [_circleView.layer addAnimation:animate3 forKey:nil];
        
    });
    
}

- (void)stop {
    [_circleView.layer removeAllAnimations];
    [_timer invalidate];
    _timer = nil;
    
}




@end
