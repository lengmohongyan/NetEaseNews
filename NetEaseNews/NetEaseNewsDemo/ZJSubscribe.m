//
//  ZJSubscribe.m
//  NetEaseNewsDemo
//
//  Created by Xwoder on 15/6/4.
//  Copyright (c) 2015年 Lining. All rights reserved.
//

#import "ZJSubscribe.h"
#import "LNNetworkManager.h"
#import "NSObject+LNExtension.h"
#import "ZJSubscribeList.h"


@interface ZJSubscribe ()

@end

@implementation ZJSubscribe

+ (void)loadSubscribesWithURLStr:(NSString *)urlStr finished:(void (^)(NSArray *))finished {
    NSLog(@"%@", urlStr);
    
    // 断言提示必须传入回调参数
    NSAssert(finished, @"必须传入完成回调");
    
    [[LNNetworkManager sharedManager] GET:urlStr parameters:nil success:^(NSURLSessionDataTask *task, NSDictionary *responseObject) {
        NSMutableArray *arrayM = [NSMutableArray arrayWithCapacity:responseObject.count];
        for (NSDictionary *dict in responseObject) {
            ZJSubscribe *subscribe = [ZJSubscribe subscribeWithDictionary:dict];
            [arrayM addObject:subscribe];
        }
        finished(arrayM);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@", error);
    }];
}

// 字典转模型方法
- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    if (self) {
        self.cName = dict[@"cName"];
        self.cid = dict[@"cid"];
        self.tList = [ZJSubscribeList listWithDictArray:dict[@"tList"]];
    }
    return self;
}

+ (instancetype)subscribeWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%@ - %@ - %@", self.cName, self.cid, self.tList];
}

@end
