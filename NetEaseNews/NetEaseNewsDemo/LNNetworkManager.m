//
//  LNNetworkManager.m
//  NetEaseNewsDemo
//
//  Created by Lining on 15/5/11.
//  Copyright (c) 2015年 Lining. All rights reserved.
//

#import "LNNetworkManager.h"

@implementation LNNetworkManager
+ (instancetype)sharedManager {
    static LNNetworkManager *manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSURL *baseURL = [NSURL URLWithString:@"http://c.m.163.com/nc/article/headline/"];
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
        manager = [[self alloc] initWithBaseURL:baseURL sessionConfiguration:config];
        // 设置响应数据格式
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @" text/javascript", @"text/html", nil];
    });
    return manager;
}
@end
