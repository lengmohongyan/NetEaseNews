//
//  LNNewsTableViewController.m
//  NetEaseNewsDemo
//
//  Created by Lining on 15/5/11.
//  Copyright (c) 2015年 Lining. All rights reserved.
//

#import "LNNewsTableViewController.h"
#import "LNNews.h"
#import "LNNewsCell.h"
#import "ZJRefreshControl.h"

@interface LNNewsTableViewController ()

/// 新闻模型数组
@property (nonatomic, strong) NSArray *newsList;

@end

@implementation LNNewsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupRefreshControl];
}

- (void)setupRefreshControl {
    // 自定义下拉刷新控件
    ZJRefreshControl *refreshControl = [[ZJRefreshControl alloc] init];
    self.refreshControl = refreshControl;
    
    [refreshControl addTarget:self action:@selector(func) forControlEvents:UIControlEventValueChanged];
    
}


- (void)func {
    // 开始刷
    [self.refreshControl beginRefreshing];
}

#pragma mark - 加载新闻数据
- (void)setUrlStr:(NSString *)urlStr {
    _urlStr = urlStr.copy;
    self.newsList = nil;
    // 网络异步加载数据
    [LNNews loadNewsListWithURLStr:urlStr finished:^(NSArray *newsList) {
        self.newsList = newsList;
        ZJRefreshControl * refreshControl = (ZJRefreshControl *)self.refreshControl;
        [refreshControl endRefreshing];
    }];
}

- (void)setNewsList:(NSArray *)newsList {
    _newsList = newsList;
    [self.tableView reloadData];
}

#pragma mark - tableView数据源方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.newsList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LNNews *news = self.newsList[indexPath.row];
    NSString *reuseId = [LNNewsCell cellReuseIdWithNews:news];
    LNNewsCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseId forIndexPath:indexPath];
    cell.news = news;
    return cell;
}

#pragma mark - tableView代理方法
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [LNNewsCell rowHeightWithNews:self.newsList[indexPath.row]];
}

@end
