//
//  ZJSubscribeList.m
//  NetEaseNewsDemo
//
//  Created by Xwoder on 15/6/4.
//  Copyright (c) 2015年 Lining. All rights reserved.
//

#import "ZJSubscribeList.h"

@implementation ZJSubscribeList

// 字典转模型
- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    if (self != nil) {
        self.tname = dict[@"tname"];
        self.subnum = dict[@"subnum"];
        self.img = dict[@"img"];
    }
    return self;
}


+ (instancetype)listWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

+ (NSArray *)listWithDictArray:(NSArray *)dictArray {

    NSMutableArray *arrayM = [NSMutableArray arrayWithCapacity:dictArray.count];
    for (NSDictionary *dict in dictArray) {
        // 每个dict对应一个字典格式的可订阅的栏目
        ZJSubscribeList *list = [ZJSubscribeList listWithDictionary:dict];
        [arrayM addObject:list];
    }
    return arrayM;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%@ - %@, %@", self.tname, self.subnum, self.img];
}

@end
