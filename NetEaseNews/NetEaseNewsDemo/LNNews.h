//
//  LNNews.h
//  NetEaseNewsDemo
//
//  Created by Lining on 15/5/11.
//  Copyright (c) 2015年 Lining. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LNNews : NSObject
///  标题
@property (nonatomic, copy) NSString *title;
///  摘要
@property (nonatomic, copy) NSString *digest;
///  跟贴数量
@property (nonatomic, assign) NSInteger replyCount;
///  配图地址
@property (nonatomic, copy) NSString *imgsrc;
///  大图标记
@property (nonatomic, assign) BOOL imgType;
///  多图数组
@property (nonatomic, strong) NSArray *imgextra;
///  阅读-我的阅读-cell
@property (nonatomic, copy) NSString *source;
///  阅读频道的多图数组
@property (nonatomic, strong) NSArray *imgnewextra;
///  视听频道的 视频缩略图
@property (nonatomic, copy) NSString *cover;
/**
 *  加载 urlStr 对应的新闻模型数组
 */
+ (void)loadNewsListWithURLStr:(NSString *)urlStr finished:(void (^)(NSArray *newsList))finished;
@end
