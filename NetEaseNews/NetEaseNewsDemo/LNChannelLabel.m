//
//  LNChannelLabel.m
//  NetEaseNewsDemo
//
//  Created by Lining on 15/5/11.
//  Copyright (c) 2015年 Lining. All rights reserved.
//

#import "LNChannelLabel.h"
#define NormarlLabelSize    14.0
#define SelectedLabelSize   18.0

@implementation LNChannelLabel

+ (instancetype)labelWithTitle:(NSString *)title {
    
    LNChannelLabel *label = [[self alloc] init];
    
    label.text = title;
    // 设置大字体
    label.font = [UIFont systemFontOfSize:SelectedLabelSize];
    label.textAlignment = NSTextAlignmentCenter;
    [label sizeToFit];
    
    // 设置小字体
    label.font = [UIFont systemFontOfSize:NormarlLabelSize];
    label.userInteractionEnabled = YES;
    
    return label;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if ([self.delegate respondsToSelector:@selector(channelLabelDidSelected:)]) {
        [self.delegate channelLabelDidSelected:self];
    }
}

- (void)setScale:(float)scale {
    _scale = scale;
    // 1. 计算最大变化比例
    float max = SelectedLabelSize / NormarlLabelSize - 1;
    // 2. 根据 scale 计算实际变化比例
    float percent = max * scale + 1;
    
    // 3. 设置形变
    self.transform = CGAffineTransformMakeScale(percent, percent);
    
    // 4. 设置颜色
    self.textColor = [UIColor colorWithRed:scale green:0.0 blue:0.0 alpha:1.0];
}
@end
