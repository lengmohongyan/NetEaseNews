//
//  ZJRefreshControl.m
//  NetEaseNewsDemo
//
//  Created by Xwoder on 15/6/2.
//  Copyright (c) 2015年 Lining. All rights reserved.
//

#import "ZJRefreshControl.h"
#import "ZJRefreshView.h"

@interface ZJRefreshControl ()

@property (weak, nonatomic) ZJRefreshView *refreshView;

@end

@implementation ZJRefreshControl

- (instancetype)init {
    self = [super init];
    if (self) {
        [self prepareRefreshView];
    }
    return self;
}

- (void)prepareRefreshView {
    ZJRefreshView *refreshView = (ZJRefreshView *)[[[NSBundle mainBundle] loadNibNamed:@"ZJRefreshView" owner:nil options:nil] firstObject];
    [self addSubview:refreshView];
    self.refreshView = refreshView;
}

- (void)beginRefreshing {
    [super beginRefreshing];
    
    [self.refreshView start];
}

- (void)endRefreshing {
    [super endRefreshing];
    
    [self.refreshView stop];
}




@end
