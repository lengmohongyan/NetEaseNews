//
//  ChannelEditCollectionViewController.m
//  ChannelEdit
//
//  Created by Lining on 15/6/4.
//  Copyright (c) 2015年 lengmolehongyan. All rights reserved.
//

#import "ChannelEditCollectionViewController.h"
#import "LNChannel.h"
#import "ChannelEditCell.h"

@interface ChannelEditCollectionViewController () <ChannelEditCellDelegate, UIGestureRecognizerDelegate>
/**
 *  频道列表数组
 */
@property (nonatomic, strong) NSMutableArray *channelList;

@end

@implementation ChannelEditCollectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 添加长按手势
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(becomeDeleteMode:)];
    longPress.delegate = self;
    [self.collectionView addGestureRecognizer:longPress];
}

#pragma mark - collectionView数据源方法
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.channelList.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ChannelEditCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ChannelEditCell" forIndexPath:indexPath];
    cell.channel = self.channelList[indexPath.item];
    cell.delegate = self;
    
    if (cell.deleteButton.hidden == NO) {
        
        UIPanGestureRecognizer *gesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(move:)];
        gesture.delegate = self;
        [cell addGestureRecognizer:gesture];
    }
    return cell;
}

#pragma mark - collectionView代理方法
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
}

#pragma mark - 手势代理方法
- (void)move:(UILongPressGestureRecognizer *)gesture {
    
    // 初始 fromIndexPath
    static NSIndexPath *fromIndexPath = nil;
    
    // 判断手势
    switch (gesture.state) {
            
        case UIGestureRecognizerStateBegan:{
            NSIndexPath *toIdxPath = [self.collectionView indexPathForItemAtPoint:[gesture locationInView:self.collectionView]];
            fromIndexPath = toIdxPath;
        }
            
        case UIGestureRecognizerStateChanged:{
            
            NSIndexPath *toIdxPath = [self.collectionView indexPathForItemAtPoint:[gesture locationInView:self.collectionView]];
            if (toIdxPath && ![toIdxPath isEqual:fromIndexPath]) {
                [self.channelList exchangeObjectAtIndex:toIdxPath.item withObjectAtIndex:fromIndexPath.item];
                [self.collectionView moveItemAtIndexPath:fromIndexPath toIndexPath:toIdxPath];
                fromIndexPath = toIdxPath;
            }
        }
            break;
        case UIGestureRecognizerStateEnded:{
            
            NSIndexPath *toIdxPath = [self.collectionView indexPathForItemAtPoint:[gesture locationInView:self.collectionView]];
            if (toIdxPath && ![toIdxPath isEqual:fromIndexPath]) {
                [self.channelList exchangeObjectAtIndex:toIdxPath.item withObjectAtIndex:fromIndexPath.item];
                [self.collectionView moveItemAtIndexPath:fromIndexPath toIndexPath:toIdxPath];
                [self.collectionView reloadData];
            }
        }
            break;
        default:
            break;
    }
}

/**
 *  删除模式
 */
- (void)becomeDeleteMode:(UILongPressGestureRecognizer *)gesture {
    
    // 遍历 collectionView 子控件，显示删除按钮
    [self.collectionView.subviews enumerateObjectsUsingBlock:^(UIView *view, NSUInteger idx, BOOL *stop) {
        if ([view isKindOfClass:[ChannelEditCell class]]) {
            
            ChannelEditCell *cell = (ChannelEditCell *)view;
            cell.deleteButton.hidden = NO;
            [self.collectionView reloadData];
        }
    }];
}

#pragma mark - channelEditCell代理方法
- (void)channelEditCell:(ChannelEditCell *)cell didClickDeleteButton:(UIButton *)deleteButton {
    
    NSIndexPath *idxPath = [self.collectionView indexPathForCell:cell];
    [self.channelList removeObject:self.channelList[idxPath.item]];
    [self.collectionView reloadData];
}

#pragma mark - 懒加载
- (NSMutableArray *)channelList {
    if (_channelList == nil) {
        _channelList = [LNChannel channelsList];
    }
    return _channelList;
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

@end

