//
//  ZJSubscribe.h
//  NetEaseNewsDemo
//
//  Created by Xwoder on 15/6/4.
//  Copyright (c) 2015年 Lining. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZJSubscribe : NSObject

@property (copy, nonatomic) NSArray *tList;
@property (copy, nonatomic) NSString *cName;
@property (copy, nonatomic) NSString *cid;

//+ (NSArray *)subscribeList;

// 字典转模型方法
- (instancetype)initWithDictionary:(NSDictionary *)dict;
+ (instancetype)subscribeWithDictionary:(NSDictionary *)dict;

+ (void)loadSubscribesWithURLStr:(NSString *)urlStr finished:(void (^)(NSArray *))finished;

@end
