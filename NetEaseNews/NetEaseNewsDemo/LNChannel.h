//
//  LNChannel.h
//  NetEaseNewsDemo
//
//  Created by Lining on 15/5/11.
//  Copyright (c) 2015年 Lining. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LNChannel : NSObject
///  频道名称
@property (nonatomic, copy) NSString *tname;
///  频道代号
@property (nonatomic, copy) NSString *tid;
/// url地址
@property (nonatomic, copy) NSString *urlStr;

/**
 *  频道模型数组
 */
+ (NSMutableArray *)channelsList;
@end
