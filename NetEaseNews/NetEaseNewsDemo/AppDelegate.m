//
//  AppDelegate.m
//  NetEaseNewsDemo
//
//  Created by Lining on 15/5/11.
//  Copyright (c) 2015年 Lining. All rights reserved.
//

#import "AppDelegate.h"
#import <AFNetworkActivityIndicatorManager.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    // 做整个项目中的全局配置
    [self setupNavgationBarStyle];
    
    // 设置缓存策略
    [self setupURLCache];
    // 设置联网指示器
    [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
    
    // 设置状态栏样式
    application.statusBarStyle = UIStatusBarStyleLightContent;
    return YES;
}

/**
 *  设置导航栏样式
 */
- (void)setupNavgationBarStyle {
    // 统一设置导航栏的颜色
    UINavigationBar *bar = [UINavigationBar appearance];
    // top_navigation_background_88 top_navigation_background
    [bar setBackgroundImage:[UIImage imageNamed:@"top_navigation_background"] forBarMetrics:UIBarMetricsDefault];
    // 设置导航栏标题样式
    bar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
//    bar.barTintColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"top_navigation_background"]];
//    bar.tintColor = [UIColor whiteColor];
}

/**
 *  设置缓存策略
 */
- (void)setupURLCache {
    NSURLCache *cache = [[NSURLCache alloc] initWithMemoryCapacity:4 * 1024 * 1024 diskCapacity:20 * 1024 * 1024 diskPath:nil];
    [NSURLCache setSharedURLCache:cache];
}


@end
