//
//  LNChannelCell.h
//  NetEaseNewsDemo
//
//  Created by Lining on 15/5/11.
//  Copyright (c) 2015年 Lining. All rights reserved.
//

#import <UIKit/UIKit.h>
@class LNNewsTableViewController;
@interface LNChannelCell : UICollectionViewCell
///  新闻视图控制器
@property (nonatomic, strong) LNNewsTableViewController *newsVC;
///  新闻地址 URL 字符串
@property (nonatomic, copy) NSString *urlStr;
@end
