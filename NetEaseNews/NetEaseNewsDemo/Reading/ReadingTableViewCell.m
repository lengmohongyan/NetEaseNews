//
//  ReadingTableViewCell.m
//  NetEaseNewsDemo
//
//  Created by mac on 15/6/4.
//  Copyright (c) 2015年 Lining. All rights reserved.
//

#import "ReadingTableViewCell.h"
#import <UIImageView+AFNetworking.h>
#include "LNNews.h"
#define kImgExtraCellID @"three"
#define kImgBigCellID   @"big"
#define kImgNorCellID   @"small"
#define kImgExtraRH 300
#define kImgBigH    240
#define kImgNorH    140

@interface ReadingTableViewCell()
@property (weak, nonatomic) IBOutlet UIImageView *smallOneIconView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;
@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *imagesExtra;

@end

@implementation ReadingTableViewCell
///计算可重用id
+(NSString *)cellIdentifier:(LNNews *)news
{
    
    if (news.imgnewextra.count == 2) {
        return kImgExtraCellID;
    } else if (news.imgType) {
        return kImgBigCellID;
    }
    return kImgNorCellID;

}
/// 计算行高
+ (CGFloat)rowHeightWithNews:(LNNews *)news {
    if (news.imgnewextra.count == 2) {
        return kImgExtraRH;
    } else if (news.imgType) {
        return kImgBigH;
    }
    return kImgNorH;
}
///给cell赋值
-(void)setNews:(LNNews *)news
{
    self.titleLabel.text = news.title;
    self.userNameLabel.text = news.source;
    self.detailLabel.text = news.digest;
    if (news.imgnewextra.count == 2) {
        __block NSInteger index = 0;
        [self.imagesExtra enumerateObjectsUsingBlock:^(UIImageView *imgView, NSUInteger idx, BOOL *stop) {
            imgView.image = nil;
            [imgView setImageWithURL:[NSURL URLWithString:news.imgnewextra[index][@"imgsrc"]]];
            NSLog(@"11111111111111111111111%@",imgView.image);
            index++;
        }];
    }
    [self.smallOneIconView setImageWithURL:[NSURL URLWithString:news.imgsrc]];
}


@end
