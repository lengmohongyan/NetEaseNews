//
//  ReadingTableViewController.m
//  NetEaseNewsDemo
//
//  Created by mac on 15/6/4.
//  Copyright (c) 2015年 Lining. All rights reserved.
//

#import "ReadingTableViewController.h"
#import "LNNews.h"
#import "ReadingTableViewCell.h"
@interface ReadingTableViewController ()
@property (nonatomic, strong) NSArray *newsList;
@end
@implementation ReadingTableViewController


///加载数据
- (void)viewDidLoad {
    [super viewDidLoad];
    [LNNews loadNewsListWithURLStr:@"http://c.3g.163.com/recommend/getSubDocPic?passport=9ee9539a0c7e8e0894e046dfbfc869f1@tencent.163.com&devId=3CA0E30A-D537-48F0-A0FD-6D31FC0BED08&size=6&from=yuedu" finished:^(NSArray *newsList) {
        self.newsList = newsList;
    }];
    
}

///设置新闻对象数组
-(void)setNewsList:(NSArray *)newsList
{
    _newsList = newsList;
    [self.tableView reloadData];
}
#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return  self.newsList.count;

}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    LNNews *news =self.newsList[indexPath.row];

    NSString *ID = [ReadingTableViewCell cellIdentifier:news];
    NSLog(@"%@",ID);
    ReadingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID forIndexPath:indexPath];

    cell.news = news;
    return cell;
}

#pragma mark - tableView代理方法
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [ReadingTableViewCell rowHeightWithNews:self.newsList[indexPath.row]];
}

@end
