//
//  ReadingChannelCell.m
//  reading
//
//  Created by mac on 15/6/3.
//  Copyright (c) 2015年 mac. All rights reserved.
//

#import "ReadingChannelCell.h"
#import "ReadingTableViewController.h"
@interface ReadingChannelCell()
@property (nonatomic, strong) ReadingTableViewController *readingTVC;
@end
@implementation ReadingChannelCell
/// 加载tableViewController
-(void)awakeFromNib
{
    NSLog(@"%@",NSStringFromCGRect(self.bounds));
    UIStoryboard *sb =[UIStoryboard storyboardWithName:@"Reading" bundle:nil];
    self.readingTVC = [sb instantiateViewControllerWithIdentifier:@"reading"];
    [self addSubview:_readingTVC.tableView];
    
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    _readingTVC.view.frame = self.bounds;
}


@end
