//
//  ReadingTableViewCell.h
//  NetEaseNewsDemo
//
//  Created by mac on 15/6/4.
//  Copyright (c) 2015年 Lining. All rights reserved.
//

#import <UIKit/UIKit.h>
@class LNNews;
@interface ReadingTableViewCell : UITableViewCell


@property (nonatomic, strong) LNNews *news;

///根据模型对象返回不同的id
+(NSString *)cellIdentifier:(LNNews *)news;
/**
 *  根据news模型设置行高
 */
+ (CGFloat)rowHeightWithNews:(LNNews *)news;

@end
