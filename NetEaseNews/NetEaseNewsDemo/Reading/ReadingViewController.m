//
//  ReadingViewController.m
//  reading
//
//  Created by mac on 15/6/3.
//  Copyright (c) 2015年 mac. All rights reserved.
//

#import "ReadingViewController.h"
#import "ReadingChannelCell.h"
#import "channelChangedView.h"

@interface ReadingViewController ()
/// 阅读界面的布局对象
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *ChannelLayout;
/// 频道切换的titleView
@property (nonatomic, strong) channelChangedView *titleView;


#define chanelNumber 2 // 频道数
#define changeReadingCellNotification @"changeReadingCellNotification"//通知名
@end

@implementation ReadingViewController

///初始化阅读频道的布局
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.titleView = self.titleView;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeChannelCell:) name:changeReadingCellNotification object:nil];
}


-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    CGFloat h = [UIScreen mainScreen].bounds.size.height - 109;
    CGFloat w = [UIScreen mainScreen].bounds.size.width;
    _ChannelLayout.itemSize = CGSizeMake(w, h);
    _ChannelLayout.minimumInteritemSpacing =0 ;
    _ChannelLayout.minimumLineSpacing = 0;
    _ChannelLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    
}

/// 通知中心实现点击滑块切换频道
-(void)changeChannelCell:(NSNotification*)n
{
    
    [self.collectionView scrollToItemAtIndexPath:n.object atScrollPosition:UICollectionViewScrollPositionNone animated:YES];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {

    return chanelNumber;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
    ReadingChannelCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"readingcell" forIndexPath:indexPath];
    
    return cell;
}

/// 实现titleview滑块滚动
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
//    NSLog(@"%@",NSStringFromCGPoint(self.collectionView.contentOffset));
    CGFloat offsetx = self.collectionView.contentOffset.x;
    self.titleView.offsetX = offsetx;
   
}

///懒加载阅读和推荐的titleview
-(channelChangedView *)titleView
{
    if (_titleView == nil)
    {
        _titleView = [[[NSBundle mainBundle] loadNibNamed:@"changeChannel" owner:nil options:nil] lastObject];
    }
    return _titleView;
}
@end
