//
//  LNChannelLabel.h
//  NetEaseNewsDemo
//
//  Created by Lining on 15/5/11.
//  Copyright (c) 2015年 Lining. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LNChannelLabel;
// 定义协议
@protocol LNChannelLabelDelegate <NSObject>
@optional
/**
 *  点击了频道标签
 */
- (void)channelLabelDidSelected:(LNChannelLabel *)label;
@end

@interface LNChannelLabel : UILabel
/**
 *  自定义频道label视图
 *
 *  @param title 标题 频道名
 */
+ (instancetype)labelWithTitle:(NSString *)title;

///  标签缩放比例，0～1.0，0是14号字黑色，1.0是18号字红色
@property (nonatomic, assign) float scale;
@property (nonatomic, weak) id<LNChannelLabelDelegate> delegate;
@end
