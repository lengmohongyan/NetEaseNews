//
//  VideoAndListenTVCCell.m
//  NetEaseNewsDemo
//
//  Created by mac on 15/6/6.
//  Copyright (c) 2015年 Lining. All rights reserved.
//

#import "VideoAndListenTVCCell.h"
#import <UIImageView+AFNetworking.h>
#import "LNNews.h"
@interface VideoAndListenTVCCell()

@property (weak, nonatomic) IBOutlet UIImageView *coverIconView;

@end
@implementation VideoAndListenTVCCell


-(void)setNews:(LNNews *)news
{
    
    [_coverIconView setImageWithURL:[NSURL URLWithString:news.cover]];
    
}

@end
