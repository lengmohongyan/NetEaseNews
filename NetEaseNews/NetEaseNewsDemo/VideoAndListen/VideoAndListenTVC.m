//
//  VideoAndListenTVC.m
//  NetEaseNewsDemo
//
//  Created by mac on 15/6/6.
//  Copyright (c) 2015年 Lining. All rights reserved.
//

#import "VideoAndListenTVC.h"
#import "LNNews.h"
#import "VideoAndListenTVCCell.h"
@interface VideoAndListenTVC ()
@property (nonatomic, strong) NSArray *videoList;
@end

@implementation VideoAndListenTVC

-(void)setVideoList:(NSArray *)videoList
{
    
    _videoList = videoList;
    [self.tableView reloadData];
}


- (void)viewDidLoad {
    [super viewDidLoad];
   // 加载数据
    [LNNews loadNewsListWithURLStr:@"http://c.m.163.com/nc/video/list/V9LG4B3A0/y/0-10.html" finished:^(NSArray *newsList) {
        self.videoList = newsList;
    }];
    
 }


#pragma mark - Table view data source



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {


    return self.videoList.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    
    VideoAndListenTVCCell *cell = [tableView dequeueReusableCellWithIdentifier:@"VideoAndListenTVCCell" forIndexPath:indexPath];
    LNNews *news = self.videoList[indexPath.row];
    cell.news = news;
    

    
    return cell;
}




@end
