//
//  VideoAndListenTVCCell.h
//  NetEaseNewsDemo
//
//  Created by mac on 15/6/6.
//  Copyright (c) 2015年 Lining. All rights reserved.
//

#import <UIKit/UIKit.h>
@class LNNews;
@interface VideoAndListenTVCCell : UITableViewCell
@property (nonatomic, strong) LNNews *news;

@end
