//
//  VLChnnelChangedView.m
//  NetEaseNewsDemo
//
//  Created by mac on 15/6/6.
//  Copyright (c) 2015年 Lining. All rights reserved.
//

#import "VLChnnelChangedView.h"
@interface VLChnnelChangedView()
/// 白色滑块
@property (weak, nonatomic) IBOutlet UIImageView *slideer;
///
#warning 等待处理字体颜色渐变
@property (nonatomic, assign) CGFloat movingX;
/// 实现上下联动
@property (nonatomic, assign) CGFloat channelLabelColorX;
/// 点击我的阅读
- (IBAction)myreadingClik;
/// 点击推荐阅读
- (IBAction)suposeReading;
/// 我的阅读按钮
@property (weak, nonatomic) IBOutlet UIButton *myreadingLabel;
/// 推荐阅读按钮
@property (weak, nonatomic) IBOutlet UIButton *suposeReadingLabel;
#define VLchangeReadingCellNotification @"VLchangeReadingCellNotification"
@end

@implementation VLChnnelChangedView

/// 实现上下联动和文字颜色渐变
-(void)setOffsetX:(CGFloat )offsetX
{
    if( offsetX<0 || offsetX > [UIScreen mainScreen].bounds.size.width) return;
    _offsetX = offsetX;
    _movingX = (offsetX * 90) / [UIScreen mainScreen].bounds.size.width;
    _slideer.transform = CGAffineTransformMakeTranslation(_movingX, 0);
    
    _channelLabelColorX = offsetX - [UIScreen mainScreen].bounds.size.width *0.5;
    NSLog(@"LABELCOLORLABELCOLORLABELCOLORLABELCOLORLABELCOLOR%f",_channelLabelColorX);
    
}

/// 实现点击button切换我的阅读和推荐阅读
- (IBAction)myreadingClik {
    NSIndexPath *path =[NSIndexPath indexPathForRow:0 inSection:0];
    [[NSNotificationCenter defaultCenter] postNotificationName:VLchangeReadingCellNotification object:path];
}
- (IBAction)suposeReading {
    NSIndexPath *path =[NSIndexPath indexPathForRow:1 inSection:0];
    [[NSNotificationCenter defaultCenter] postNotificationName:VLchangeReadingCellNotification object:path];
}
@end;
