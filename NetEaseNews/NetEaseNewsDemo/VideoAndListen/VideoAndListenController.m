//
//  VideoAndListenController.m
//  NetEaseNewsDemo
//
//  Created by mac on 15/6/6.
//  Copyright (c) 2015年 Lining. All rights reserved.
//

#import "VideoAndListenController.h"
#import "VideoAndListenFatherCell.h"
#import "VLChnnelChangedView.h"

@interface VideoAndListenController ()<UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UICollectionView *VideoAndListenCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *VideoAndListenFatherCollectitonViewFlowLayout;
@property (nonatomic, strong) VLChnnelChangedView *titleView;
#define VLchangeReadingCellNotification @"VLchangeReadingCellNotification"//通知名
@end

@implementation VideoAndListenController




- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.titleView = self.titleView;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(VLchangeChannelCell:) name:VLchangeReadingCellNotification object:nil];

}
/// 通知中心实现点击滑块切换频道
-(void)VLchangeChannelCell:(NSNotification*)n
{
    
    [self.VideoAndListenCollectionView scrollToItemAtIndexPath:n.object atScrollPosition:UICollectionViewScrollPositionNone animated:YES];
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    _VideoAndListenFatherCollectitonViewFlowLayout.itemSize = self.VideoAndListenCollectionView.bounds.size;
    _VideoAndListenFatherCollectitonViewFlowLayout.minimumInteritemSpacing=0;
    _VideoAndListenFatherCollectitonViewFlowLayout.minimumLineSpacing=0;
    _VideoAndListenFatherCollectitonViewFlowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    _VideoAndListenCollectionView.pagingEnabled=YES;
    _VideoAndListenCollectionView.showsHorizontalScrollIndicator=NO;
    self.automaticallyAdjustsScrollViewInsets=NO;
    
}


-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 2;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    VideoAndListenFatherCell *cell = [self.VideoAndListenCollectionView dequeueReusableCellWithReuseIdentifier:@"VideoAndListenFatherCell" forIndexPath:indexPath];
    
    return cell;
    
}
/// 实现titleview滑块滚动
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    //    NSLog(@"%@",NSStringFromCGPoint(self.collectionView.contentOffset));
    CGFloat offsetx = self.VideoAndListenCollectionView.contentOffset.x;
    self.titleView.offsetX = offsetx;
    
}
///懒加载阅读和推荐的titleview
-(VLChnnelChangedView *)titleView
{
    if (_titleView == nil)
    {
        _titleView = [[[NSBundle mainBundle] loadNibNamed:@"VLChangeChannel" owner:nil options:nil] lastObject];
    }
    return _titleView;
}
@end
