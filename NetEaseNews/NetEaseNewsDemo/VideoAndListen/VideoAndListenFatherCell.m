//
//  VideoAndListenFatherCell.m
//  NetEaseNewsDemo
//
//  Created by mac on 15/6/6.
//  Copyright (c) 2015年 Lining. All rights reserved.
//

#import "VideoAndListenFatherCell.h"
#import "VideoAndListenTVC.h"
@interface VideoAndListenFatherCell()

@property (nonatomic, strong) VideoAndListenTVC *TVC;

@end
@implementation VideoAndListenFatherCell


-(void)awakeFromNib
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"VideoAndListen" bundle:nil];
     _TVC = sb.instantiateInitialViewController;
    [self addSubview:_TVC.view];
    
}


-(void)layoutSubviews
{
    _TVC.view.frame=self.bounds;
    
}
@end
