//
//  LNImportNewsTableViewController.m
//  NetEaseNewsDemo
//
//  Created by Lining on 15/5/12.
//  Copyright (c) 2015年 Lining. All rights reserved.
//

#import "LNImportNewsTableViewController.h"

@interface LNImportNewsTableViewController ()
- (IBAction)backBtnClick:(UIButton *)sender;

@end

@implementation LNImportNewsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}



- (IBAction)backBtnClick:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
