//
//  LNChannel.m
//  NetEaseNewsDemo
//
//  Created by Lining on 15/5/11.
//  Copyright (c) 2015年 Lining. All rights reserved.
//

#import "LNChannel.h"
#import "NSObject+LNExtension.h"

@implementation LNChannel

+ (NSMutableArray *)channelsList {
    NSURL *fileURL = [[NSBundle mainBundle] URLForResource:@"topic_news.json" withExtension:nil];
    NSData *data = [NSData dataWithContentsOfURL:fileURL];
    NSDictionary *channelsDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:NULL];
    NSArray *array = channelsDict[channelsDict.keyEnumerator.nextObject];
    NSMutableArray *channelsList = [NSMutableArray arrayWithCapacity:array.count];
    [array enumerateObjectsUsingBlock:^(NSDictionary *dict, NSUInteger idx, BOOL *stop) {
        
        [channelsList addObject:[self objectWithDict:dict]];
    }];
    // 对频道数组排序
    NSMutableArray *arrayM = [NSMutableArray arrayWithArray:
                              [channelsList sortedArrayUsingComparator:^NSComparisonResult(LNChannel *obj1, LNChannel *obj2) {
        return [obj1.tid compare:obj2.tid];
    }]];
    return arrayM;
//    return channelsList.copy;
}

/**
 *  重写 tid setter 方法，同时设置 URLString
 */
- (void)setTid:(NSString *)tid {
    _tid = tid.copy;
    _urlStr = [NSString stringWithFormat:@"%@/0-20.html", tid];
}
@end
